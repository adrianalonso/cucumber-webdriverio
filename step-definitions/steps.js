const { Given, When, Then } = require('cucumber');

Given('I am on the Cloud District homepage', () => {
    return browser.url("https://clouddistrict.com")
});

When(/^I click on "([^"]*)?"$/,  (text) => {
    return browser.click('a='+text)
});

When(/^I wait "([^"]*)?" seconds$/,  (number) => {
    return browser.pause(parseInt(number)*1000)
});

Then(/^I should see the title "([^"]*)?"$/,  (text) => {
    return browser.element('h2='+text)
});

Then(/^I should see the success messsage "([^"]*)?"$/,  (text) => {
    var result= browser.element('h4='+text)

    if(result.value === null){
        throw new Error("Message not found")
    }
});

Then(/^I should see the error messsage "([^"]*)?"$/,  (text) => {
    var result= browser.element('p.val-error-text='+text)

    if(result.value === null){
        throw new Error("Error Message not found")
    }
});

Then(/^I fill "([^"]*)?" with "([^"]*)?"$/,  (element, value) => {
    return browser.setValue('*[name="'+element+'"]',value)
});

Then(/^I accept policy$/,  () => {
    return browser.click('.val-input-box.check label')
});

Then(/^I submit contact form$/,  () => {
    return browser.click('form input[type="submit"]')
});