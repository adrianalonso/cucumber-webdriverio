
# Cucumber Webdriver.io Example

This repository is a Gherkin, Webdriver.io and Cucumber.io example to show end to end tests.

## HOW TO RUN

1. docker-compose up
2. docker-compose exec dev-node /bin/bash
3. ./node_modules/.bin/wdio


## REPL MODE
node_modules/.bin/wdio repl chrome --host hub