Feature: Form Contact Feature
  As a Anonymous user
  I want to contact with Cloud District
  In order to contract a new project

 Scenario: User fill a form and no accept policy
    Given I am on the Cloud District homepage
    When I click on "Contacto"
    Then I should see the title "Contáctanos"
    When I fill "contact-name" with "Adrián"
    And I fill "contact-email" with "alonsus91@gmail.com"
    And I fill "contact-business" with "My Company"
    And I fill "contact-mobile" with "616364750"
    And I fill "contact-message" with "Hello Cloud"
    And I submit contact form
    And I wait "5" seconds
    Then I should see the error messsage "Por favor, acepta la Política de Privacidad"

  Scenario: User fill a complete form and new lead is sended
    Given I am on the Cloud District homepage
    When I click on "Contacto"
    And I should see the title "Contáctanos"
    And I fill "contact-name" with "Adrián"
    And I fill "contact-email" with "alonsus91@gmail.com"
    And I fill "contact-business" with "My Company"
    And I fill "contact-mobile" with "616364750"
    And I fill "contact-message" with "Hello Cloud"
    And I accept policy
    And I submit contact form
    And I wait "5" seconds
    Then I should see the success messsage "Gracias por tu interés"
